# language: pt

Funcionalidade: Recuperar Login
	Como usuário de e-commerce
	Para fazer login e comprar no site
	Eu quero recuperar meus dados de acesso a minha conta

	Cenário: Esqueci meu e-mail
		Dado que eu esteja no site do Walmart
		E clico no menu "Meus Pedidos"
		E eu clico no link "Esqueci meu e-mail" 
		E eu preencho o CPF "XXX"
		E eu preencho o CEP "01415-000"
		Entao eu devo ver a mensagem "O e-mail registrado em nosso cadastro é:"
		E eu devo ver a mensagem "karolxavierleite@gmail.com"