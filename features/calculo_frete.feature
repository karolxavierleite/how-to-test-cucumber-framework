# language: pt

Funcionalidade: Cálculo de Frete
	Como usuário de e-commerce
	Para estimar o valor das minhas compras
	Eu quero poder calcular o frete em diversos pontos do site, antes de avançar com o pedido

	Cenário: Calcular frete no carrinho
		Dado que eu esteja no site do Walmart
		E eu adiciono um produto qualquer no carrinho
		E eu preencho o campo de CEP com "01415-000"
		E clico no botão "Calcular"
		Entao eu devo ver o "Valor do Frete" com o valor "Frete Grátis"


